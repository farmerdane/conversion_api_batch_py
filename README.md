
# Conversion API Integration

Application that creates R&F Dynamic Ads feed and campaigns off of existing tearsheets/Rolex csv inputs.

## Requirements
- Python 3
- Facebook Business SDK for Python v10+

## Facebook Business SDK for Python - Library Dependencies

The easiest way to install the SDK is via ``pip`` in your shell.

**NOTE**: For Python 3, use ``pip3`` and ``python3`` instead.

**NOTE**: Use ``sudo`` if any of these complain about permissions. (This might
happen if you are using a system installed Python.)

If you don't have pip:

```
easy_install pip
```

Now execute when you have pip:

```
pip install facebook_business
```

https://github.com/facebook/facebook-python-business-sdk

## Config File
A `config.py` file needs to be created as it contains the settings for the access tokens and pixel IDs.

```
BATCH_SIZE=1000
ACCESS_TOKEN="replace_with_access token"
PIXEL_ID="replace_with_pixel_id"
```

**TODO: Will need to change the config and/or the application to support multiple pixels and access tokens for various country setups.**