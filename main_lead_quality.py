import time
import config

from facebook_business.adobjects.serverside.action_source import ActionSource
# from facebook_business.adobjects.serverside.content import Content
# from facebook_business.adobjects.serverside.custom_data import CustomData
from facebook_business.adobjects.serverside.event import Event
from facebook_business.adobjects.serverside.event_request import EventRequest
from facebook_business.adobjects.serverside.user_data import UserData
from facebook_business.adobjects.serverside.custom_data import CustomData
from facebook_business.api import FacebookAdsApi


class FacebookService:
    def __init__(self, access_token, pixel_id):
        self.api = FacebookAdsApi.init(access_token=access_token)
        self.pixel_id = pixel_id

    def chunks(self, l, n):
        n = max(1, n)
        return (l[i:i + n] for i in range(0, len(l), n))

    def sendBatchedData(self, data, batchsize=1000):
        events = []
        for row in data:
            try:
                user_data = UserData(
                    email=row["em"],
                    phone=row["ph"],
                    first_name=row["fn"],
                    last_name=row["ln"],
                    country_code=row["country"],
                    external_id=row["external_id"],
                    client_ip_address=row["client_ip_address"],
                    client_user_agent=row["client_user_agent"],
                    fbc=row["fbc"],
                    fbp=row["fbp"],
                )

                custom_properties = {'event_source': row["event_source"], 'lead_event_source': row["lead_event_source"]}
                custom_data = CustomData(
                    custom_properties=custom_properties
                )

                # preemptive call this to kick back any issues for the row and do not add to event payload
                # TypeError is thrown if there is invalid data
                user_data.normalize()

                event = Event(
                    event_name=row["event_name"],
                    event_time=row["conversion_date"],
                    event_id=row["external_id"],
                    user_data=user_data,
                    custom_data=custom_data,
                    event_source_url=row["event_source_url"],
                    action_source=row["action_source"]
                )

                events.append(event)
            except TypeError as e:
                if hasattr(e, 'message'):
                    print(e.message)
                else:
                    print(e)

        # Conversion API cannot take more than 1000 events at once
        # Break it into request chunks if it exceeds that amount
        event_chunks = self.chunks(events, batchsize)
        for event_chunk in event_chunks:
            # Event Request class will normalize the valid values
            event_request = EventRequest(
                events=event_chunk,
                pixel_id=self.pixel_id,
            )
            print(event_request.get_params())
            event_response = event_request.execute()
            print(event_response)


class TransformQuery:
    def transformQueryData(self, data):
        if not data:
            return []

        for row in data:
            row["event_name"] = "Lead"
            row["action_source"] = ActionSource.WEBSITE
            row["event_source"] = "crm"
            row["lead_event_source"] = "databricks"
        return data

if __name__ == '__main__':
    fs = FacebookService(
        access_token=config.ACCESS_TOKEN,
        pixel_id=config.PIXEL_ID
    )
    tqd = TransformQuery()

    # Query DB here and return array of the following keys/data format
    sampleData = [
        {
            "ln": "farmer",
            "fn": "dane",
            "em": "farmerdane@yahoo.com",
            "ph": "+4407530000000",
            "country": "GB",
            "external_id": "sdf-234-w3rf",
            "fbp": "fb.1.1620172064218.345966172",
            "fbc": "fb.1.1620172064217.IwAR0xA5LNUNGg2j2mXiqF-9xqC-fsMFfAWBZsJ6dciJRwlwJhtz1lqQkAJmY",
            "client_ip_address": "42.22.137.150",
            "client_user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36",
            "event_source_url": "https://phonewatch.ie/form",
            "conversion_date": int(time.time())
        },
        {
            "ln": "Smith",
            "fn": "Joe",
            "em": "js@yahoo.com",
            "ph": "+4407531111111",
            "country": "GB",
            "external_id": "udn-234-dh27",
            "fbp": "fb.1.1620172064218.345966172",
            "fbc": "fb.1.1620172064217.IwAR0xA5LNUNGg2j2mXiqF-9xqC-fsMFfAWBZsJ6dciJRwlwJhtz1lqQkAJmY",
            "client_ip_address": "125.163.81.46",
            "client_user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36",
            "event_source_url": "https://phonewatch.ie/form",
            "conversion_date": int(time.time())
        },
        {
            "ln": "Johnson",
            "fn": "Tom",
            "em": "tom@yahoobrokenemail",
            "ph": "+4407532222222",
            "country": "UK",
            "external_id": "udn-767-h7w4",
            "fbp": "fb.1.1620172064218.345966172",
            "fbc": "fb.1.1620172064217.IwAR0xA5LNUNGg2j2mXiqF-9xqC-fsMFfAWBZsJ6dciJRwlwJhtz1lqQkAJmY",
            "client_ip_address": "34.42.12.183",
            "client_user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36",
            "event_source_url": "https://phonewatch.ie/form",
            "conversion_date": int(time.time())
        }
    ]

    # enrich and/or transform any values coming from the DB for Conversion API compatibility
    sampleData = tqd.transformQueryData(sampleData)
    fs.sendBatchedData(sampleData, config.BATCH_SIZE)
